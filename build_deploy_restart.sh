#!/bin/sh
docker build -t sentiment .

docker tag sentiment 118838618954.dkr.ecr.us-east-2.amazonaws.com/news-sentiment:latest
aws ecr get-login --no-include-email | sh
docker push 118838618954.dkr.ecr.us-east-2.amazonaws.com/news-sentiment:latest
aws ecs register-task-definition --cli-input-json file://news-sentiment-task-def.json

for task in `aws ecs list-tasks  |tail -n+3 | head -n-2 | tr -d '"'`; do
    aws ecs stop-task --task "$task"
done
aws ecs run-task --task-definition news-sentiment
