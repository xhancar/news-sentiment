from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser, Profile
import sys


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = UserCreationForm.Meta.fields

class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = UserChangeForm.Meta.fields

class CustomUserForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CustomUserForm, self).__init__(*args, **kwargs)
        # assign initial values from model
        for f in self.Meta.fields:
            self.fields[f].value = getattr(self.Meta.model, f)

    class Meta:
        model = CustomUser
        fields = ()

class ProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        # assign initial values from model
        for f in self.Meta.fields:
            self.fields[f].value = getattr(self.Meta.model, f)
            self.fields[f].required = False
        self.fields['api_key'].help_text = 'get API key by <a target="_blank" href="https://newsapi.org/register">registering</a> on NewsAPI.org'
        self.fields['page_size'].help_text = 'Number of search results displayed on one page (maximum is 100).'
        
    class Meta:
        model = Profile
        fields = ('api_key', 'page_size')
