from django.db import models
from django.contrib.auth.models import UserManager, AbstractUser
from sentiment_project import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.cache import cache
from django.contrib.auth.signals import user_logged_in, user_logged_out
import telnetlib
import re

# Create your models here.

class CustomUserManager(UserManager):
    pass

class CustomUser(AbstractUser):
    objects = CustomUserManager()

class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    api_key = models.CharField(max_length=32)
    page_size = models.IntegerField(default=20, validators=[MaxValueValidator(100),MinValueValidator(1)])

@receiver(post_save, sender=CustomUser)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=CustomUser)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

def clear_cache(user):
    cache.delete_many(cache.keys('%d@*'%user.id)) 
    cache.delete_many(cache.keys('%d#*'%user.id)) 

@receiver(user_logged_in, sender=CustomUser)
def user_logged_in(sender, request, user, **kwargs):
    clear_cache(user)

@receiver(user_logged_out, sender=CustomUser)
def user_logged_out(sender, request, user, **kwargs):
    clear_cache(user)
