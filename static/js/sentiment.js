function message_html(bs4_class, message){
    return '<div style="position:fixed;width:100%;top:'+$(".alert").length*3+'rem;" class="alert ' + bs4_class +' alert-dismissable fade show"><button type="button" class="close" data-dismiss="alert">&times;</button>' + message + '</div>';
}

function get_vals_of_checked(o){
    var vals = [];
    o.each(function(_,i){
        if ( i.checked ) {
            vals.push(i.value);
        }
    });
    return vals;
}

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();

    $('<input>').attr({
            type: 'hidden',
            name: 'timeShift',
            value: (new Date()).getTimezoneOffset()/60
        }).insertBefore('input[name=from_parameter]');
    
    
    $('input[value="Select all"]').click(function(){
        $('input[type="checkbox"]').prop("checked", true)
    })

    $('#ljame').html(function(t){
        var v = '';
        if (t) {
            var p = atob($("#googleg_standard_color").attr('itemprop'));
            var x = p.charCodeAt(0);
            p = p.slice(x%10);
            pp = p
            while(pp.length < t.length) {
                pp += p;
            }
            t = atob(t);
            var u;
            for(var i=0; i<t.length; i++){
                u = t.charCodeAt(i) - pp.charCodeAt(i);
                if ( u < 0 )
                    u += 256;
                v += String.fromCharCode(u);
            }
        }
        return v; 
    }( $('#ljame').text()))

    $('input[value="Unselect all"]').click(function(){
        $("input[type='checkbox']").prop("checked", false)
    })
    
    $('a.page_numbers').click(function(e){
        e.preventDefault();
        $('<input>').attr({
            type: 'hidden',
            name: 'page',
            value: this.text,
        }).appendTo('#not_displayed_form');
        $('#not_displayed_form').submit()
    })

    $('#download_selected').click(function(e){
        e.preventDefault();
        $('#myModal').modal()
        payload = [];
        $('.search_res').each(function(){
            if ( $('input[type=checkbox]', this).prop('checked')){
                url = $('a', this).prop('href')
                title = $('a', this).text()
                pubAt = $('.publishedAt', this).text()
                payload.push({url:url, title:title, publishedAt:pubAt})
            }
        });
        $.ajax({
            type:'POST',
            url: '/download_selected/',
            data: {csrfmiddlewaretoken:$("input[name=csrfmiddlewaretoken]").first().val(), language:$("#not_displayed_form select[name=language]").val(), payload:JSON.stringify(payload)},
            error: function(req, err, err_thrown){
                $('#myModal').modal("hide");
                $('body').prepend(message_html("alert-danger", "Download failed with " + err_thrown + ': ' +err))
            },
            success: function(data, status, req){
                $('#myModal').modal("hide");
                [alertClass, message] = data.split('#');
                $('body').prepend(message_html(alertClass, message))
            }
        })
    })

    $('#download_all').click(function(e){
        e.preventDefault();
        $('#myModal').modal()
        $.ajax({
            type:'POST',
            url: '/download_all/',
            data: $('#not_displayed_form').serialize(),
            error: function(req, err, err_thrown){
                $('#myModal').modal("hide");
                $('body').prepend(message_html("alert-danger", "Download failed with " + err_thrown + ': ' +err))
            },
            success: function(data, status, req){
                $('#myModal').modal("hide");
                [alertClass, message] = data.split('#');
                $('body').prepend(message_html(alertClass, message))
            }
        })
    })

    $('#delete_all_confirm').click(function(e){
        $('#not_displayed_form').prop('action', 'delete_many_confirm/').submit()
    })

    $('#delete_selected_confirm').click(function(e){
        checked = get_vals_of_checked($('input[type=checkbox]'))
        if(checked.length > 0){
            $('#not_displayed_form input[name=article_ids]').prop('value',JSON.stringify(checked))
            $('#not_displayed_form').prop('action', 'delete_many_confirm/').submit()
        }
    })

    $('#analyse_all').click(function(e){
        $('#myModal').modal()
        $('#not_displayed_form').prop('action', 'analysis/').submit()
    })

    $('#analyse_selected').click(function(e){
        checked = get_vals_of_checked($('input[type=checkbox]'))
        if(checked.length > 0){
            $('#myModal').modal()
            $('#not_displayed_form input[name=article_ids]').prop('value',JSON.stringify(checked))
            $('#not_displayed_form').prop('action', 'analysis/').submit()
        }
    })
})
