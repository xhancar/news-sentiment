#!/bin/sh

if [ -z "`docker images -q sentiment`" -o "$1" = "-r" ]; then
    docker build -t sentiment .
    containers=`docker ps -qf ancestor=sentiment`
    if [ -n "$containers" ]; then
        docker rm -f $containers
    fi
fi

if [ -z "`docker ps -aq -f name=sentiment-postgres`" ]; then
    docker run --name sentiment-postgres  -d postgres
elif [ -z "`docker ps -q -f name=sentiment-postgres`" ]; then
    docker start sentiment-postgres 
fi

if [ -z "`docker ps -aq -f name=sentiment-redis`" ]; then
    docker run --name sentiment-redis  -d redis
elif [ -z "`docker ps -q -f name=sentiment-redis`" ]; then
    docker start sentiment-redis 
fi

#docker run  --name sentiment --link sentiment-postgres:postgres  -p 8000:8000 sentiment
docker run -v /etc/letsencrypt:/etc/letsencrypt --link sentiment-postgres --link sentiment-redis  -p 8000:8000 -p 8001:8001 sentiment
