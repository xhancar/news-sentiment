#!/bin/bash
OUT_DIR=$1
OUT_FNAME=$2
if [ -z "$OUT_DIR" -o -z "$OUT_FNAME" ]; then
    echo 'Usage: $0 OUT_DIR OUT_FNAME\n' >&2
    exit 2
fi
# Generate a passphrase
openssl rand -base64 48 > /tmp/passphrase.txt

# Generate a Private Key
openssl genrsa -aes128 -passout file:/tmp/passphrase.txt -out /tmp/server.key 2048

# Generate a CSR (Certificate Signing Request)
#openssl req -new -passin file:/tmp/passphrase.txt -key /tmp/server.key -out /tmp/server.csr \
#    -subj "/C=FR/O=krkr/OU=Domain Control Validated/CN=*.krkr.io"
openssl req -new -passin file:/tmp/passphrase.txt -key /tmp/server.key -out /tmp/server.csr \
    -subj "/C=US/O=amazonaws/OU=Domain Control Validated/CN=*.us-east-2.compute.amazonaws.com"

# Remove Passphrase from Key
cp /tmp/server.key /tmp/server.key.org
openssl rsa -in /tmp/server.key.org -passin file:/tmp/passphrase.txt -out /tmp/server.key

# Generating a Self-Signed Certificate for 100 years
openssl x509 -req -days 36500 -in /tmp/server.csr -signkey /tmp/server.key -out /tmp/server.crt

mv /tmp/server.crt $OUT_DIR/$OUT_FNAME.crt
mv /tmp/server.key $OUT_DIR/$OUT_FNAME.key
rm -f /tmp/server.csr /tmp/passphrase.txt
