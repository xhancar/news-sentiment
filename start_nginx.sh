#!/bin/sh
rm -f /etc/nginx/sites-enabled/default &&
cp /code/conf/nginx_sentiment.conf /etc/nginx/sites-enabled/sentiment &&
nginx -p /var/www -g "daemon off;"
