#!/bin/sh
D_CMD='exec -it'
CMD='bash'
if [ -n "$1" ]; then
    D_CMD=$*
    CMD=''
fi

docker $D_CMD `docker ps -q --filter ancestor=sentiment` $CMD
