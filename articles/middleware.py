from django.conf import settings
from django.core.cache import cache

class BreadcrumbsCacheMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        if request.user.is_authenticated:
            if request.path != request.get_full_path():
                cache.set('breadcrumbs_full_path:%d@%s'%(request.user.id, request.path), request.get_full_path())
            else:
                cache.delete('breadcrumbs_full_path:%d@%s'%(request.user.id, request.path))


        response = self.get_response(request)

        return response
