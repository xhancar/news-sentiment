from django import template
from datetime import datetime
from datetime import timedelta
import base64
import re

register = template.Library()

@register.filter
def arg_concat(value, arg):
    return str(value) + ',' + str(arg)

@register.filter
def format_datetime(value, time_shift):
    utc = datetime.strptime(value, "%Y-%m-%dT%H:%M:%SZ")
    local = utc - timedelta(hours=time_shift)
    return datetime.strftime(local, "%Y-%m-%d %H:%M:%S")

@register.filter
def make_bold(text, pattern):
    if pattern.strip() == '':
        return text
    else:
        return re.sub('(%s)'%re.escape(pattern), r'<b>\1</b>', text, flags=re.I)

@register.filter
def highlight(text, pattern):
    if pattern.strip() == '':
        return text
    else:
        return re.sub('(%s)'%re.escape(pattern), r'<b style="background:yellow;">\1</b>', text, flags=re.I)

@register.filter
def cut_around_occur(text, pattern):
    """ Cut text around the first occurence of pattern.
        The maximal size of text surrounding the pattern (on one of both side)
            is specified at the and of the text behind a comma.
        For example: text='A very cool text,2', pattern='cool'
                    returns '...y cool t...'
    """
    text, surr_size_s = text.rsplit(',',1)
    surr_size = int(surr_size_s)
    i = text.lower().find(pattern.lower()) 
    if i == -1 or pattern.strip() == '':
        return text[ : 2*surr_size] + '...' * int(2*surr_size < len(text))
    elif i < surr_size:
        return text[: len(pattern)+ 2*surr_size] + '...' * int(len(pattern)+2*surr_size < len(text))
    elif i >= surr_size:
        return int(surr_size<i)*'...'+ text[i-surr_size : i+len(pattern)+surr_size] + '...' * int(i+len(pattern)+surr_size < len(text))

@register.filter
def extend_name(value):
    d = { 'pos': 'Positive',
          'neg': 'Negative',
          'neu': 'Neutral',
          'compound' : 'Compound',
        }
    return d[value]

@register.filter
def encrypt(text, passwd):
    p = base64.b64decode(passwd)
    i = int(p[0]) % 10
    p = p[i:]
    p *= len(text) // len(p) + int(bool(len(text) % len(p)))
    enc_bytes = bytes([ (int(b) + int(p[j]))%256 for j,b in enumerate(bytes(text, 'utf8'))])
    return base64.b64encode(enc_bytes).decode('ascii')
