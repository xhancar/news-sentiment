from ..models import Article
from django import template
from django.core.cache import cache
from django.urls import reverse
from django.utils.html import format_html, mark_safe
from urllib.parse import urlparse
import re

register = template.Library()

bcrumb_len = 20
base_bcrump_name = 'my_articles_list'
pk_re = re.compile('article-(\d+)')

def breadcrumbs_mapping(pk=None):
    mapping = {
        reverse('my_articles') : 'My Articles',
        reverse('my_articles_list') : 'List of My Articles',
        reverse('delete_many_confirm') : 'Remove Articles',
        reverse('analysis') : 'Sentiment Analysis',
    }
    if pk:
        _title = Article.objects.get(pk=pk).title
        title = _title[:bcrumb_len] + '...' if len(_title) > bcrumb_len else _title
        mapping.update({
            reverse('article', args=[pk]) : title,
            reverse('delete_article', args=[pk]) : 'Delete Article',
            reverse('article_analysis', args=[pk]) : 'Article Analysis',
            reverse('article_from_analysis', args=[pk]) : title,
            reverse('delete_article_from_analysis', args=[pk]) : 'Delete Article',
            reverse('analyse_article_from_analysis', args=[pk]) : 'Article Analysis',
        })
    return mapping


@register.simple_tag(takes_context=True)
def breadcrumbs(context, url_path):
    user = context['request'].user
    u = urlparse(url_path)
    path = u.path
    pk = pk_re.search(path).group(1) if pk_re.search(path) else None
    mapping = breadcrumbs_mapping(pk)
    _breadcrumbs = [ mapping[path] ]
    while True:
        if path == reverse(base_bcrump_name):
            break
        path, _ = path.rstrip('/').rsplit('/', 1)
        path += '/'
        full_path = cache.get('breadcrumbs_full_path:%d@%s'%(user.id, path))
        if not full_path:
            full_path = path
        _breadcrumbs.append(format_html('<a href="{}">{}</a>', full_path, mapping[path]))
    return mark_safe("&nbsp;&raquo; ".join(reversed(_breadcrumbs)))
