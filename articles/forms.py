from django import forms
from .models import GoogNews
import datetime

class NewsApiForm(forms.ModelForm):
    def __init__(self, request):
        super(NewsApiForm,self).__init__()
        # assign initial values from request, if not then from model
        import sys
        for f in self.Meta.fields:
            self.fields[f].value =  getattr(self.Meta.model, f)
            if f in request.POST:
                self.fields[f].initial = request.POST[f]
            self.fields[f].required = False
        self.fields['from_parameter'].label = 'From :'
    class Meta:
        model = GoogNews
        fields = ('query', 'from_parameter', 'to', 'sort_by', 'language') 
