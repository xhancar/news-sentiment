from django.urls import path, include
from . import views

urlpatterns = [
    path( '', views.home_view, name='home'),
    path( 'about', views.AboutPageView.as_view(), name='about'),
    path( 'page_expired', views.PageExpiredView.as_view(), name='page_expired'),

    path( 'goog_news/', views.goog_news_view, name='goog_news'),
    path( 'download_all/', views.download_all_view, name='download_all'),
    path( 'download_selected/', views.download_selected_view, name='download_selected'),

    path( 'my_articles/', views.my_articles_view, name='my_articles'),
    path( 'my_articles/list/', views.MyArticlesListView.as_view(), name='my_articles_list'),
    path( 'my_articles/list/delete_many_confirm/', views.delete_many_confirm_view, name='delete_many_confirm'),
    path( 'my_articles/list/delete_many_confirm/delete_many', views.delete_many_view, name='delete_many'),

    path( 'my_articles/list/article-<int:pk>/', views.ArticleView.as_view(), name='article'),
    path( 'my_articles/list/article-<int:pk>/delete', views.DeleteArticleView.as_view(), name='delete_article'),
    path( 'my_articles/list/article-<int:pk>/analysis', views.analysis_view, name='article_analysis'),

    path( 'my_articles/list/analysis/', views.analysis_view, name='analysis'),

    path( 'my_articles/list/analysis/article-<int:pk>/', views.ArticleView.as_view(), name='article_from_analysis'),
    path( 'my_articles/list/analysis/article-<int:pk>/delete', views.DeleteArticleView.as_view(), name='delete_article_from_analysis'),
    path( 'my_articles/list/analysis/article-<int:pk>/analysis', views.analysis_view, name='analyse_article_from_analysis'),

]

