from django.db import models
import datetime
from pycountry import languages

class GoogNews(models.Model):
    today = datetime.date.today().strftime('%Y-%m-%d') + ' 00:00'
    tomorrow = (datetime.date.today() + datetime.timedelta(days=1)).strftime('%Y-%m-%d') + ' 00:00'
    SORT_BY_CHOICES = [('date', 'date') ,('relevancy', 'relevancy'), ('popularity', 'popularity')]
    
    # Languages supproted by NewsAPI.org
    #supported_langs = ['ar', 'de', 'en', 'es', 'fr', 'he', 'it', 'nl', 'no', 'pt', 'ru', 'se', 'zh']
    #LANGUAGE_CHOICES = [('','All')] + [(lang, languages.get(alpha_2=lang).name) for lang in supported_langs] # newsapi response in all languages by default, but the value must be '' (not 'all' etc.)

    # Languages supported by SentimentIntensityAnalyzer of NLTK Vader.
    supported_langs = ['en']
    LANGUAGE_CHOICES = [(lang, languages.get(alpha_2=lang).name) for lang in supported_langs]

    query = models.CharField(max_length=500)
    from_parameter = models.DateTimeField(default=today)
    to = models.DateTimeField(default=tomorrow)
    sort_by = models.CharField(max_length=10, choices=SORT_BY_CHOICES, default="relevancy")
    language = models.CharField(max_length=2, choices=LANGUAGE_CHOICES, default="")
  

class Article(models.Model):
    url = models.URLField(db_index=True, max_length = 2000)
    user = models.ForeignKey('users.CustomUser', on_delete=models.CASCADE)
    title = models.CharField(max_length = 400, default='')
    publishedAt = models.DateTimeField()
    language = models.CharField(max_length = 2)
    text = models.TextField()

    class Meta:
        unique_together = (('user', 'url'),)
