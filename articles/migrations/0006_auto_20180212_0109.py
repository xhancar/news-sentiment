# Generated by Django 2.0.1 on 2018-02-12 01:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0005_auto_20180211_0945'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='title',
            field=models.CharField(default='', max_length=400),
        ),
        migrations.AlterField(
            model_name='goognews',
            name='from_parameter',
            field=models.DateTimeField(default='2018-02-12 00:00'),
        ),
        migrations.AlterField(
            model_name='goognews',
            name='to',
            field=models.DateTimeField(default='2018-02-13 00:00'),
        ),
    ]
