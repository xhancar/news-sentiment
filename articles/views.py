from django.shortcuts import render
from django.views.generic import ListView, DeleteView, DetailView, TemplateView
from django.contrib import messages
import logging
import sys
logging.basicConfig(level=logging.DEBUG)

import psycopg2

from .models import Article

from .forms import NewsApiForm

from users.models import Profile

from django.db.utils import IntegrityError

from django.shortcuts import redirect

from django.http.response import HttpResponse, HttpResponseRedirect

from django.urls import reverse, reverse_lazy

from django.conf import settings
import nltk
if settings.NLTK_DATA_DIR not in nltk.data.path:
    nltk.data.path.append(settings.NLTK_DATA_DIR)
from nltk.sentiment.vader import SentimentIntensityAnalyzer
try:
    _ = SentimentIntensityAnalyzer()
except:
    nltk.download('vader_lexicon', download_dir=settings.NLTK_DATA_DIR)
from nltk.tokenize import sent_tokenize
try:
    _=sent_tokenize('')
except LookupError:
    nltk.download('punkt', download_dir=settings.NLTK_DATA_DIR) 


from datetime import  datetime
from datetime import  timedelta
from django.core.cache import cache
from django.db.models import Q
from django.template.loader import render_to_string
from pycountry import languages
from pyhashxx import hashxx
import base64
import bisect
import grequests
from newsapi import NewsApiClient
import json
import justext
import lxml
import os
import queue
import random
import re
import requests
import sqlite3
import string
import tempfile
import threading
import time
import urllib

CACHE_TIMEOUT=35*60 # seconds
MAX_REQ_CONCURRENTLY = 1000

import resource
resource.setrlimit(resource.RLIMIT_NOFILE, (2 * MAX_REQ_CONCURRENTLY, 2 * MAX_REQ_CONCURRENTLY))

with open('countries.json', 'r') as f:
    countries = json.load(f)
TLDS_LANGNAMES = { c['tld'][0] : list(c['languages'].values())[0] for c in countries if c['tld'] and c['languages']}

with open('useragents.txt', 'r') as f: 
    USER_AGENTS = [line.strip() for line in f]

def home_view(request):
    if request.user.is_authenticated:
        return redirect('goog_news')
    else:
        return redirect('login')

def everything_worker(na_client, news_q, page_nums, **kwargs):
    while True:
        try:
            kwargs['page'] = page_nums.get(timeout=1)
            news_q.put(na_client.get_everything(**kwargs))
            time.sleep(1+2*random.random())
        except queue.Empty:
            break

def get_news_api_results(api_key, _all=False, **kwargs):
    """  kwargs correspond to request items of NewsAPI, but 'q' can be both 'q' and 'query'
    """
    kwargs['from_parameter'] = datetimestr_to_utcstr(kwargs['from_parameter'][0], int(kwargs['timeShift'][0]))
    kwargs['to'] = datetimestr_to_utcstr(kwargs['to'][0], int(kwargs['timeShift'][0]))

    if 'query' in kwargs and not 'q' in kwargs:
        kwargs = dict(kwargs)
        kwargs['q'] = kwargs['query']
        kwargs.pop('query')

    kwargs.pop('timeShift')
    kwargs.pop('csrfmiddlewaretoken')
 
    
    if _all:
        kwargs['page_size'] = 100 # 100 is maximum
        kwargs['page'] = 1


    na_client = NewsApiClient(api_key=api_key)
    news_res = na_client.get_everything(**kwargs)
    napi_thread_num = 18 # about 20 requests at the same time caused 'error, rateLimited, You have made too many requests recently. ...
    page_nums = queue.Queue()
    page_sum = news_res['totalResults']//kwargs['page_size'] + ( 1 if news_res['totalResults'] % kwargs['page_size'] else 0 )
    for i in range(2, page_sum + 1):
        page_nums.put(i)
    if _all and news_res['status'] == 'ok':
        page_size_sum = len(news_res['articles'])
        news_q = queue.Queue()
        for _ in range(min(napi_thread_num, news_res['totalResults'])):
            t = threading.Thread(target=everything_worker, args=[na_client, news_q, page_nums], kwargs=kwargs)
            t.start()

        while True:
            try:
                nr = news_q.get(timeout=5)
            except queue.Empty:
                break 
            try:
                news_res['articles'] += nr['articles']
            except KeyError as e:
                logging.debug('KEY_ERR'+str(e)+str(dir(e))+' '+repr(nr))
            if nr['status'] != 'ok':
                news_res['status'] = nr['status']
                news_res['code'] = nr['code']
                news_res['message'] = news_res['message'] + '\n' + nr['message'] if 'message' in news_res else nr['message']
    
    return news_res

def str_to_datetime(s):
    s = s.strip()
    try:
        dt = datetime.strptime(s, '%Y-%m-%d %H:%M:%S')
    except ValueError:
        try:
            dt = datetime.strptime(s+':00', '%Y-%m-%d %H:%M:%S')
        except ValueError:
            try:
                dt = datetime.strptime(s+':00:00', '%Y-%m-%d %H:%M:%S')
            except ValueError:
                try:
                    dt = datetime.strptime(s+' 00:00:00', '%Y-%m-%d %H:%M:%S')
                except ValueError:
                    raise ValueError('Invalid format of date and time "%s"'%s)
    return dt

def datetimestr_to_utcstr(s, time_shift):
    dt = str_to_datetime(s)
    if dt is None:
        return None
    else:
        return (dt + timedelta(hours=time_shift)).strftime('%Y-%m-%dT%H:%M:%SZ')     

def get_tab(path):
    if path.startswith('/my_articles'):
        return 'my_articles'
    elif path.startswith('/goog_news'):
        return 'news'
    else:
        return None

def cache_key(request):
    return '%d@%d'%(request.user.id, hashxx(request.get_full_path().encode('utf8')))

def cache_rev_key(request, page_name):
    return '%d@%d'%(request.user.id, hashxx(reverse(page_name).encode('utf8')))
    

def set_cache(request, html):
    if request.user.is_authenticated:
        tab = get_tab(request.path)
        if tab is not None:
            cache.set('%d#%s'%(request.user.id, tab), request.get_full_path(), CACHE_TIMEOUT)
        cache.set(cache_key(request), html, CACHE_TIMEOUT)
    
def cache_render(request, template, context):
    view_html = render_to_string(template, context, request=request)
    cache_html = view_html
    if hasattr(request, '_messages'):
        request._messages._loaded_data = []
        cache_html = render_to_string(template, context, request=request)
    set_cache(request, cache_html) 
    return HttpResponse(view_html)

def cached_or_expired(request):
    html = cache.get(cache_key(request))
    if html:
        set_cache(request, html)
        return HttpResponse(html)
    else:
        return HttpResponseRedirect(reverse('page_expired'))

def goog_news_view(request):
    tab = 'news'
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse_lazy('login'))
    form = NewsApiForm(request)
    user_authenticated = request.user.is_authenticated
    api_key = request.user.profile.api_key
    page_size = request.user.profile.page_size
    kwargs = dict(request.POST)
    # if submitted form 
    if 'csrfmiddlewaretoken' in request.POST:
        kwargs['page_size'] = page_size
        if 'page' in kwargs and isinstance(kwargs['page'], list):
            kwargs['page'] = int(kwargs['page'].pop())
        elif not 'page' in kwargs:
            kwargs['page'] = 1
        if user_authenticated and api_key:
            news_results = get_news_api_results(api_key, **kwargs)
            if news_results['status'] == 'ok':
                return cache_render(request, 'goog_news.html',
                                { 'total_results': news_results['totalResults'],
                                  'tab':'news',
                                  'search_results': news_results['articles'],
                                  'form': form,
                                  'time_shift': int(kwargs['timeShift'][0]),
                                  'page_numbers': range(1, 1 + news_results['totalResults']//page_size + int(bool(news_results['totalResults']%page_size))),
                                  'page': kwargs['page'],
                                  'increase_possible':str_to_datetime(kwargs['to'][0]) >= datetime.now() + timedelta(hours=int(kwargs['timeShift'][0]))
                                }
                             )
            else:
                messages.error(request, news_results['message'])
        elif not api_key:
            messages.error(request, 'You need to get your API key by <a href="https://newsapi.org/register" class="alert-link">registering</a> on NewsAPI.org and save it to your profile.')
    else:
        url = cache.get('%d#%s'%(request.user.id, tab))
        if url:
            html = cache.get(cache_key(request))
            if html:
                set_cache(request, html)
                return HttpResponse(html)
    return cache_render(request, 'goog_news.html', {'tab':tab, 'form': form, 'search_results':[]})

def langname_from_TLD(url):
        tld_re = re.compile(r"\.[a-z]{2,}(?=/|$)")
        tld = tld_re.search(url).group(0)
        return TLDS_LANGNAMES.get(tld, 'English') 

def extract_text(html, lang, url):
    langname = languages.get(alpha_2=lang).name if lang != '' else langname_from_TLD(url)
    stoplist = {}
    try:
        stoplist = justext.get_stoplist(langname)
    except ValueError:
        try:
            stoplist = justext.get_stoplist(langname.split()[-1]) # "Swiss German" -> "German"
        except ValueError:
            stoplist = justext.get_stoplist('English')
    paragraphs = justext.justext(html, stoplist)
    return '\n\n'.join([p.text for p in paragraphs if p.class_type == 'good'])

def secure_bulk_create(Model, model_objects):
    num_saved = 0
    try:
        Model.objects.bulk_create(model_objects)
        num_saved = len(model_objects)
    except (IntegrityError, psycopg2.IntegrityError): 
        if len(model_objects) > 50:
            num_saved += secure_bulk_create(Model, list(model_objects)[:len(model_objects)//2])
            num_saved += secure_bulk_create(Model, list(model_objects)[len(model_objects)//2:])
        else:
            for obj in model_objects:
                try:
                    obj.save()
                    num_saved += 1
                except IntegrityError:
                    logging.info("object not saved:" + repr(obj))
    return num_saved

def req_exception_handler(request, exception):
    logging.info("Request failed (%s) because %s"%(request.url, repr(exception)))

def quote_path(url):
    url_parts = list(urllib.parse.urlsplit(url))
    url_parts[2] = urllib.parse.quote(url_parts[2])
    return urllib.parse.urlunsplit(url_parts)
	
def download_and_extract(search_results, lang, user):
    urls = []
    url_idx = {}
    downloaded_before = 0
    for res in search_results:
        url = quote_path(res['url'])
        if Article.objects.filter(url=url).filter(user=user).exists():
            downloaded_before += 1
        else:
            urls.append(url)    
            url_idx[url] = { 'url':url,
                                    'user':user,
                                    'title':res['title'],
                                    'publishedAt':res['publishedAt'].replace('T',' ').strip(),
                                    'language':lang,
                                  }
    if not urls:
        return 0, downloaded_before
    req_concurrently = MAX_REQ_CONCURRENTLY if len(urls) > MAX_REQ_CONCURRENTLY else len(urls)
    responses = grequests.imap((grequests.get(u) for u in urls), exception_handler=req_exception_handler, size=req_concurrently)
    num_downloaded = 0
    new_articles = {}
    logging.debug('ORIGINAL URLS')
    logging.debug(str(urls))
    for resp in responses:
        url = resp.history[0].url if resp.history else resp.url
        kw = url_idx[url]
        kw['title'] = '' if kw['title'] is None else kw['title']
        try:
            kw['text'] = extract_text(resp.content, lang, resp.url)
            if kw['text'].strip() == '':
                logging.info("No text extracted from %s -- omitted"%url)
                continue
        except (lxml.etree.ParserError, lxml.etree.XMLSyntaxError) as e:
            logging.info("Parsing of (%s) failed because %s"%(url, repr(e)))
            continue
        new_articles[resp.url] = Article(**kw)
        if len(new_articles) >= 1000:
            num_downloaded += secure_bulk_create(Article, new_articles.values())
            new_articles={}
    if new_articles:
        num_downloaded += secure_bulk_create(Article, new_articles.values())
    return num_downloaded, downloaded_before
    
def format_ajax_response(num_downloaded, downloaded_before, total):
    assert(num_downloaded + downloaded_before <= total)
    if num_downloaded + downloaded_before < total:
        return HttpResponse('alert-warning#Only {}  of {} articles were downloaded ({} downloaded now, {} earlier)'.format(num_downloaded + downloaded_before, total, num_downloaded, downloaded_before))
    elif num_downloaded == 0 and downloaded_before == total:
        return HttpResponse('alert-info#Nothing new downloaded: All the {} articles were downloaded earlier.'.format(downloaded_before))
    else:
        return HttpResponse('alert-success#All {} articles were successfully downloaded ({} now, {} earlier)'.format(total, num_downloaded, downloaded_before))


def download_all_view(request):
    user_authenticated = request.user.is_authenticated
    api_key = request.user.profile.api_key
    if user_authenticated and api_key:
        news_results = get_news_api_results(api_key, _all=True, **request.POST)
    if news_results['status'] == 'ok':
        logging.debug('TOTAL: '+ str(news_results['totalResults']) + ', ' + str(len(news_results['articles'])))
        num_downloaded, downloaded_before = download_and_extract(news_results['articles'], request.POST['language'], request.user)
        total = news_results['totalResults']
        cache.delete("%d#%s"%(request.user.id,'my_articles'))
        cache.delete(cache_rev_key(request, 'my_articles_list'))
        return format_ajax_response(num_downloaded, downloaded_before, total)
    elif not user_authenticated:
        return HttpResponse('Authentication needed!', status=403)
    else:
        resp = HttpResponse(news_results['message'])
        resp.status_code = 500
        return resp
        

def download_selected_view(request):
    logging.debug(' download_selected_view request'+ repr(request) )
    user_authenticated = request.user.is_authenticated
    if user_authenticated:
        lang = request.POST['language']
        payload = json.loads(request.POST['payload'])
        num_downloaded, downloaded_before = download_and_extract(payload, lang, request.user)
        total = len(payload)
        cache.delete("%d#%s"%(request.user.id,'my_articles'))
        cache.delete(cache_rev_key(request, 'my_articles_list'))
        return format_ajax_response(num_downloaded, downloaded_before, total)
    else:
        return HttpResponse('Authentication needed!', status=403)

class MyArticlesListView(ListView):    
    model = Article
    template_name = 'my_articles_list.html'

    def get_search_type(self):
        if 'fulltext_search' in self.request.POST:
            return 'fulltext_search' 
        elif 'search_titles' in self.request.POST:
            return 'search_titles' 
        elif self.request.COOKIES.get('search_type','')=='fulltext_search':
            return 'fulltext_search' 
        elif self.request.COOKIES.get('search_type','')=='search_titles':
            return 'search_titles' 
        else:
            return ''

    def get_page(self):
        if 'page' in self.request.POST:
            return int(self.request.POST['page'])
        elif not 'csrfmiddlewaretoken' in  self.request.POST: # not form submission
            return int(self.request.COOKIES.get('page',1) )
        else:
            return 1

    def get_queryset(self):
        logging.debug('MyArticlesView.get_queryset'+str(self.request.POST))
        queryset = Article.objects.filter(user=self.request.user)
        str_to_search = self.request.POST['str_to_search'] if 'str_to_search' in self.request.POST else self.request.COOKIES.get('str_to_search', '')
        search_type = self.get_search_type()
        if search_type == 'search_titles':
            queryset = queryset.filter(title__icontains=str_to_search)
        elif search_type == 'fulltext_search':
            queryset = queryset.filter(Q(title__icontains=str_to_search)|Q(text__icontains=str_to_search))
        self.article_ids = [ article.pk for article in queryset.all() ]
        page_size = self.request.user.profile.page_size 
        page = self.get_page()
        return queryset[ (page-1)*page_size : page*page_size ]

    def get_context_data(self, **kwargs):
        page_size = self.request.user.profile.page_size 
        str_to_search = self.request.POST['str_to_search'] if 'str_to_search' in self.request.POST else self.request.COOKIES.get('str_to_search', '')
        data = super().get_context_data(**kwargs)
        data['last_str_to_search'] = str_to_search
        data['search_type'] = self.get_search_type()
        data['page'] = self.get_page()
        data['article_ids'] = self.article_ids
        found_articles_num = len(self.article_ids) # before slicing
        data['found_articles_num'] = found_articles_num
        data['page_numbers'] = range(1, 1 + found_articles_num //page_size + int(bool(found_articles_num%page_size)))
        data['tab'] = 'my_articles'
        return data

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.method == 'GET':
                html = cache.get(cache_key(request))
                if html:
                    set_cache(request, html) 
                    return HttpResponse(html)
            self.object_list = self.get_queryset()
            context = self.get_context_data()
            resp = cache_render(request, self.template_name, context)
            resp.set_cookie('str_to_search', value=context['last_str_to_search'])
            resp.set_cookie('page', value=context['page'])
            if 'search_type' in context:
                resp.set_cookie('search_type', context['search_type'])
            return resp
        else:
            return HttpResponseRedirect(reverse_lazy('login'))

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)

def my_articles_view(request):
    if request.user.is_authenticated: 
        url = cache.get('%d#%s'%(request.user.id, 'my_articles'))
        if url:
            return HttpResponseRedirect(url)
        else:
            return HttpResponseRedirect(reverse_lazy('my_articles_list'))
    else:
        return HttpResponseRedirect(reverse_lazy('login'))

def delete_many_confirm_view(request):
    if request.user.is_authenticated and request.method == 'GET':
        return cached_or_expired(request)
    elif request.user.is_authenticated and request.POST and 'article_ids' in request.POST:
        return cache_render(request, 'delete_many_confirm.html',
                        {
                         'tab':'my_articles',
                         'article_ids':request.POST['article_ids'],
                         'num_of_articles':len(json.loads(request.POST['article_ids'])),
                         })
    elif not request.user.is_authenticated:
        return HttpResponseRedirect(reverse_lazy('login'))
    else:
        return HttpResponseRedirect(reverse_lazy('my_articles_list'))

def delete_many_view(request):
    if request.user.is_authenticated and request.POST and 'article_ids' in request.POST:
        ids = json.loads(request.POST['article_ids'])
        Article.objects.filter(user=request.user).filter(pk__in=ids).delete()
        msg = '1 article was successfully deleted' if len(ids) == 1 else "%d articles were sucessfully deleted"%len(ids)
        messages.success(request, msg)
        cache.delete(cache_rev_key(request, 'my_articles_list'))
        return HttpResponseRedirect(reverse_lazy('my_articles_list'))
    elif not request.user.is_authenticated:
        return HttpResponseRedirect(reverse_lazy('login'))
    else:
        return HttpResponseRedirect(reverse_lazy('my_articles_list'))

class HighestScorePairs(object):
    """ Object contains max_length of pairs (score, data) with the highest score.
        They are sorted.
    """
    def __init__(self, max_length):
        self.max_len = max_length
        self.scores = []
        self.data_list = []

    def add(self, score, data):
        """Add a pair (score, data) if score is among the max_length number of the highest
           ones.
        """ 
        if len(self.scores) < self.max_len or score > self.scores[0]:
            i = bisect.bisect(self.scores, score)
            self.scores[i:i] = [score]
            self.scores = self.scores[-self.max_len:]
            self.data_list[i:i] = [data]
            self.data_list = self.data_list[-self.max_len:]
            
    def values(self):
        """Returns the pairs with highest score in decreasing order"""
        return list(zip(reversed(self.scores), reversed(self.data_list))) # two reverses, because zip object is not reversible

def analysis_view(request, pk=None):
    # GET from breadcrumbs
    if request.user.is_authenticated and request.method == 'GET' and not pk:
        return cached_or_expired(request)
    elif request.user.is_authenticated and (pk or request.POST and 'article_ids' in request.POST):
        #import cProfile, pstats, io
        #pr = cProfile.Profile()
        #pr.enable()
        ids = [pk] if pk else json.loads(request.POST['article_ids'])
        queryset = Article.objects.filter(user=request.user).filter(pk__in=ids)
        sid = SentimentIntensityAnalyzer()
        page_size = request.user.profile.page_size
        positive_sentences = HighestScorePairs(page_size)
        negative_sentences = HighestScorePairs(page_size)
        positive_articles = HighestScorePairs(page_size)
        negative_articles = HighestScorePairs(page_size)
        sentence_counter = 0
        ps_sum = {'compound':0, 'neg':0, 'neu':0, 'pos':0}
        for article in queryset:
            article_ps_sum = {'compound':0, 'neg':0, 'neu':0, 'pos':0}
            article_s_counter = 0
            for sentence in sent_tokenize(article.text):
                ps = sid.polarity_scores(sentence)
                # key_f is language specific for En! (questionmark removed with other punctuation is OK, because question have different word order in English unlike Russian etc.)
                key_f = lambda x: x.strip().rstrip(string.punctuation)

                pos_sen_keys = [key_f(sen) for sen,_ in positive_sentences.data_list]
                if key_f(sentence) in pos_sen_keys:
                    i = pos_sen_keys.index(key_f(sentence))
                    positive_sentences.data_list[i][1].add(article)
                else:
                    positive_sentences.add(ps['pos'], (sentence, {article}))


                neg_sen_keys = [key_f(sen) for sen,_ in negative_sentences.data_list]
                if key_f(sentence) in neg_sen_keys:
                    i = neg_sen_keys.index(key_f(sentence))
                    negative_sentences.data_list[i][1].add(article)
                else:
                    negative_sentences.add(ps['neg'], (sentence, {article}))

                ps_sum = { k:v+ps[k] for k,v in ps_sum.items() } 
                article_ps_sum = { k:v+ps[k] for k,v in article_ps_sum.items() } 
                sentence_counter += 1
                article_s_counter += 1
            if article_s_counter > 0:
                positive_articles.add(article_ps_sum['pos']/article_s_counter, article)
                negative_articles.add(article_ps_sum['neg']/article_s_counter, article)
        if sentence_counter > 0:
            ps_sum = { k:v/sentence_counter for k,v in ps_sum.items() } 

        #pr.disable()
        #stream = io.StringIO()
        #pst = pstats.Stats(pr, stream=stream).sort_stats('cumulative')
        #pst.print_stats()
        #pst = pstats.Stats(pr, stream=stream).sort_stats('tottime')
        #pst.print_stats()
        #logging.debug('PROFILE\n'+stream.getvalue())
        return cache_render(request, 'analysis.html',{'tab':'my_articles','ps_sum':ps_sum, 'positive_sentences':positive_sentences.values(), 'negative_sentences':negative_sentences.values(), 'positive_articles':positive_articles.values(), 'negative_articles':negative_articles.values()})
    elif not request.user.is_authenticated:
        return HttpResponseRedirect(reverse_lazy('login'))
    else:
        return HttpResponseRedirect(reverse_lazy('my_articles_list'))
    

class ArticleView(DetailView):
    model = Article
    template_name = 'article.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tab'] = 'my_articles'
        return context
        
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        context['highlight_str'] = self.request.GET.get('highlight', '')
        return cache_render(request, 'article.html', context)


class DeleteArticleView(DeleteView):
    model = Article
    template_name = "delete_article.html"
    success_url = reverse_lazy('my_articles_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tab'] = 'my_articles'
        return context

    def get(self, request, *args, **kwargs):
        cache.delete("%d#%s"%(request.user.id,'my_articles'))
        cache.delete(cache_rev_key(request, 'my_articles_list'))
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        return cache_render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        resp = super().post(request, *args, **kwargs)
        messages.success(request, 'Article successfully deleted.')
        return resp
        


class AboutPageView(TemplateView):
    template_name = 'about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['passwd'] = base64.b64encode(bytes([random.randint(0,255) for _ in range(32)])).decode('ascii')
        return context

class PageExpiredView(TemplateView):
    template_name = 'page_expired.html'
