#!/bin/sh
domain=$1
if [ -z "$domain" -o "$domain" = "-h" -o "$domain" = '--help' ]; then
    echo "Usage: $0 DOMAIN" >&2
    exit 2
fi
certbot certonly --webroot -w /var/www -d "$domain" -d "www.$domain"
