FROM python:3.6

ENV PYTHONUNBUFFERED 1

COPY . /code/
WORKDIR /code/

RUN pip install --upgrade pip &&\
    pip install --no-cache-dir -r /code/requirements.txt

RUN echo 'deb http://ftp.debian.org/debian jessie-backports main' > /etc/apt/sources.list.d/jessie-backports.list &&\
    apt-get update &&\
    apt-get install -y nginx supervisor openssh-server less vim.tiny &&\
    update-rc.d ssh defaults &&\
    service nginx stop &&\
    apt-get install certbot -yt jessie-backports &&\
    mkdir -p /var/www/.well-known/acme-challange &&\
    chown -R www-data:www-data /var/www/.well-known &&\
    cp conf/supervisor_sentiment.conf /etc/supervisor/conf.d/sentiment.conf
    #mkdir /etc/nginx/ssl &&\
    #./gen_ssl.sh /etc/nginx/ssl news-sentiment 
#COPY static/ /var/www/static
RUN python manage.py collectstatic
RUN chown -R www-data:www-data /var/www/static &&\
    find /var/www/static -type d | xargs  chmod 500 &&\
    find /var/www/static -type f | xargs  chmod 400

EXPOSE 8000 8001 22

#RUN python manage.py makemigrations --noinput
#RUN python manage.py migrate --noinput
CMD python manage.py makemigrations --noinput; python manage.py migrate --noinput; supervisord -n
